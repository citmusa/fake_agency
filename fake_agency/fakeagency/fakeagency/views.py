#coding: utf-8
from annoying.decorators import render_to
from django.core.mail import send_mail
from testimonials.models import Testimonial
from portfolio.models import Work, Category

@render_to('home.html')
def home(request):
	"""
	Home view
	"""
	#all works having active category or not
	works = Work.objects.filter(active=True).order_by('created_at')[:4]
	test = Testimonial.objects.filter(active=True).order_by('updated_at')[:4]
	return {'testimonials':test, 'works':works}


@render_to('about.html')
def about(request):
	"""
	About view
	"""
	test = Testimonial.objects.filter(active=True).order_by('updated_at')[:4]
	return {'testimonials':test}


@render_to('portfolio.html')
def portfolio(request):
	"""
	Portfolio view
	"""
	#all categories required
	categories = Category.objects.filter(active=True)
	#all works having active category or not
	works = Work.objects.filter(active=True).order_by('created_at')
	return {'categories':categories, 'works':works}


@render_to('services.html')
def services(request):
	"""
	Services view
	"""
	return {}