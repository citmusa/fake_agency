from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'fakeagency.views.home', name='home'),
    url(r'^portfolio/$', 'fakeagency.views.portfolio' , name='portfolio'),
    url(r'^services/$', 'fakeagency.views.services' , name='services'),
    url(r'^about/$', 'fakeagency.views.about' , name='about'),
    url(r'^contact/$', 'contact.views.contact' , name='contact'),

    url(r'^testimonials/$', 'testimonials.views.testimonials' , name='testimonials'),


    url(r'^404$', TemplateView.as_view(template_name='404.html')),
    url(r'^500$', TemplateView.as_view(template_name='500.html')),

    url(r'^myadmin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^myadmin/', include(admin.site.urls)),

    # Uncomment the admin/doc line below to enable admin documentation:
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))