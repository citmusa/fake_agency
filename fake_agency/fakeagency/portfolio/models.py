from django.db import models


class Category(models.Model):
	"""
	Categories from a work
	"""
	name = models.CharField(max_length=100)
	icon_class = models.CharField(max_length=50, blank=True)

	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name_plural = "Categories"

	def __unicode__(self):
		return self.name

class Work(models.Model):
	"""
	Model to store individual works
	Useful for portfolio
	"""

	title = models.CharField(max_length=100)
	description = models.TextField(max_length=250, blank=True)
	image = models.ImageField(upload_to="portfolio/")

	category = models.ManyToManyField(Category, blank=True, null=True)

	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.title
