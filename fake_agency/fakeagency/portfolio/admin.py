from django.contrib import admin
from models import Work, Category

class WorkAdmin(admin.ModelAdmin):
	"""
	Admin class for :model:`models.Work`
	"""
	list_display = ('title','active')

class CategoryAdmin(admin.ModelAdmin):
	"""
	Admin class for :model:`models.Category`
	"""
	list_display = ('name','active')

admin.site.register(Work, WorkAdmin)
admin.site.register(Category, CategoryAdmin)