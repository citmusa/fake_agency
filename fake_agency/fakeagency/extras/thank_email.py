
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils.html import strip_tags


import socket
import smtplib
timeout_value = 5.0 # seconds
socket.setdefaulttimeout(timeout_value)

def send_email():
	"""
	Function just to wrap the send email method not implemented in contact view
	Doesn't work standalone.
	"""
	try:
	    cd = form.cleaned_data 
	    subject, from_email, to = 'FakeAgency new contact', cd.get('email','noreply@example.com'),'citmusa@gmail.com'
	    
	    d = Context({ 'name':cd.get('name','Sin nombre'), 'email':cd['email'], 'message': cd['message'] })
	    
	    html = get_template('email_contact.html')
	    html_content = html.render(d)
	    text_content = strip_tags(html_content)

	    # create the email, and attach the text and html version as well.
	    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
	    msg.attach_alternative(html_content, "text/html")

	    return msg.send()
	except:
		return 0