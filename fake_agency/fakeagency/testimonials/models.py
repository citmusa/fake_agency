from django.db import models

class Testimonial(models.Model):
	"""
	Model to store testimonials
	"""
	name = models.CharField(max_length=150)
	testimonial = models.TextField(max_length=250) 
	avatar = models.ImageField("Avatar",upload_to="testimonials/")

	urlname = models.CharField(max_length=150, blank=True)
	url = models.URLField(blank=True)

	active = models.BooleanField(default=True)
   
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name