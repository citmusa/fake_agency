#coding: utf-8
from annoying.decorators import render_to
from models import Testimonial

@render_to('testimonials2.html')
def testimonials(request):
	"""
	Testimonials view to display last 4 testimonies 
	active in carousel.
	:model:`models.testimonials`
	"""
	test = Testimonial.objects.filter(active=True).order_by('updated_at')[:4]
	return {'testimonials': test}