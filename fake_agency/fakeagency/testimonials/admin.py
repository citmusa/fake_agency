#coding: utf-8
from django.contrib import admin
from models import Testimonial

class TestimonialAdmin(admin.ModelAdmin):
	"""
	Admin class for :model:`models.Testimonial`
	"""
	list_display = ('name','created_at','active')

admin.site.register(Testimonial,TestimonialAdmin)