from django import forms
from django.forms import ModelForm
from models import Contact

class ContactForm(ModelForm):
	"""
	Contact form based in Contact Model.
	Uses name, email, phone and message fields.
	"""

	class Meta:
		model = Contact
		fields=(
			"name", "email", "phone", "message",
			)

	def __init__(self, *args, **kwargs):
		#calling the original init
		super(ContactForm, self).__init__(*args,**kwargs)
		#assigning field overloads
		self.fields["name"].widget=forms.TextInput(attrs={'placeholder':'Enter your name...'})
		self.fields["email"].widget=forms.TextInput(attrs={'placeholder':'Enter your email..'})
		self.fields["phone"] = forms.RegexField(required=False,max_length=12,regex=r'[0-9]')
		self.fields["message"].widget=forms.Textarea(attrs={'placeholder':'Your message...', 'width':'','height':''})

