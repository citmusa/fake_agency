#coding: utf-8
from annoying.decorators import render_to
from forms import ContactForm

@render_to('thanks.html')
def thanks(request,name):
    """
    Thanks view for after filling the contact form.
    Receives a name.
    **Template:**
    :template:`contact/thanks.html`
    """
    return name


@render_to('contact.html')
def contact(request):
    """
    Contact view. 
    Saves a new contact in database.
    :model:`models.Contact`
    **Template:**
    :template:`contact/contact.html`
    """
    form = ContactForm()
    if request.POST:
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data 
            new_contact = form.save()
            return thanks(request,{'name': data['name']})

    return {'form': form}


