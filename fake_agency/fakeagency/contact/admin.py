#coding: utf-8
from django.contrib import admin
from models import Contact

class ContactAdmin(admin.ModelAdmin):
	"""
	Admin class for :model:`models.Contact`
	"""

	list_display = ('name','created_at','reached')

admin.site.register(Contact,ContactAdmin)