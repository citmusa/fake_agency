from django.db import models

class Contact(models.Model):
	"""
	Model to store contacts
	"""
	name = models.CharField(max_length=150)
	email = models.EmailField()
	phone = models.CharField(max_length=12, blank=True)
	message = models.TextField(max_length=250)
	
	reached = models.BooleanField(default=False) 
	active = models.BooleanField(default=True)
   
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.email